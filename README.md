# Awesome AI Setapp list

## Courses
* [Google Developers Machine Learning crash course](https://developers.google.com/machine-learning/crash-course/)
* [Coursera machine learning course](https://www.coursera.org/learn/machine-learning)
* [Hacker's guide to Neural Networks](https://karpathy.github.io/neuralnets/)
